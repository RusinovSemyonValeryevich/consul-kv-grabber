import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 10.09.2019
 * Time: 13:22
 */
public class Application {

    private static final String HELP_ARG = "-help";

    private static final String OUTPUT_DIR_ARG = "-o";

    private static final String REMOTE_HOST_ARG = "-r";

    private static String remoteHost = "localhost:8500";

    private static String outputDir = "";

    public static void main(final String[] args) {
        parseArguments(args);
        ConsulAPI consulAPI = new ConsulAPI(remoteHost);
        try {
            List<KeyValue> keyValues = consulAPI.getAllKVs();
            saveKeyValues(keyValues);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void parseArguments(final String[] args) {
        if (args.length == 1 && args[0].equals(HELP_ARG)) {
            printHelp();
        }
        if (args.length % 2 == 1) {
            System.out.println("Wrong number of arguments");
            System.exit(1);
        }
        for (int i = 0; i < args.length - 1; i += 2) {
            String argument = args[i];
            String value = args[i + 1];
            switch (argument) {
                case OUTPUT_DIR_ARG:
                    outputDir = value;
                    break;
                case REMOTE_HOST_ARG:
                    remoteHost = value;
                    break;
                default:
                    System.out.println("Unknown argument " + argument);
                    System.out.println("See -help");
                    System.exit(1);
            }
        }
    }

    private static void saveKeyValues(final List<KeyValue> keyValues) throws IOException {
        List<KeyValue> onlyWithValue =
                keyValues.stream().filter(keyValue -> keyValue.getValue() != null).collect(Collectors.toList());
        for (KeyValue keyValue : onlyWithValue) {
            File file = new File(outputDir + keyValue.getKey());
            File parentDir = file.getParentFile();
            if (!parentDir.exists()) {
                parentDir.mkdirs();
            }
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(keyValue.getValue());
            fileWriter.close();
        }
    }

    private static void printHelp() {
        System.out.println("-o {output dir}");
        System.out.println("-h {remote host}");
        System.out.println("-help print help");
        System.exit(0);
    }
}
