import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 10.09.2019
 * Time: 13:28
 */
public class ConsulAPI {

    private static final String GET_ALL_KVS_PATTERN = "http://%s/v1/kv/?recurse=true";

    private String remoteHost = "localhost:8500";

    public ConsulAPI(final String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public ConsulAPI() {
    }

    public List<KeyValue> getAllKVs() throws IOException {
        //send GET request
        URL url = new URL(String.format(GET_ALL_KVS_PATTERN, remoteHost));
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        String json = getHttpResponseString(connection);

        //parse response
        ObjectMapper objectMapper = new ObjectMapper();
        KeyValue[] result = objectMapper.readValue(json, KeyValue[].class);
        return Arrays.asList(result);
    }

    private String getHttpResponseString(final HttpURLConnection connection) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            result.append(line);
        }
        bufferedReader.close();
        return result.toString();
    }
}
