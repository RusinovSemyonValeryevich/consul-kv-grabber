import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.Base64;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 10.09.2019
 * Time: 13:33
 */
public class KeyValueDeserializer extends StdDeserializer<KeyValue> {

    public KeyValueDeserializer() {
        this(null);
    }

    private KeyValueDeserializer(final Class<?> vc) {
        super(vc);
    }

    @Override
    public KeyValue deserialize(final JsonParser jp, final DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        int lockIndex = node.get("LockIndex").asInt();
        String key = node.get("Key").asText();
        int flags = node.get("Flags").asInt();
        String value = node.get("Value").textValue();
        String decodedValue = value == null ? null : new String(Base64.getDecoder().decode(node.get("Value").textValue()));
        long createIndex = node.get("CreateIndex").asLong();
        long modifyIndex = node.get("ModifyIndex").asLong();
        return new KeyValue(lockIndex, key, flags, decodedValue, createIndex, modifyIndex);
    }
}
