import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: rsv
 * Date: 10.09.2019
 * Time: 13:31
 */
@JsonDeserialize(using = KeyValueDeserializer.class)
class KeyValue implements Serializable {

    private int lockIndex;

    private String key;

    private int flags;

    private String value;

    private long createIndex;

    private long modifyIndex;

    KeyValue(final int lockIndex, final String key, final int flags, final String value, final long createIndex,
             final long modifyIndex) {
        this.lockIndex = lockIndex;
        this.key = key;
        this.flags = flags;
        this.value = value;
        this.createIndex = createIndex;
        this.modifyIndex = modifyIndex;
    }

    public int getLockIndex() {
        return lockIndex;
    }

    public String getKey() {
        return key;
    }

    public int getFlags() {
        return flags;
    }

    public String getValue() {
        return value;
    }

    public long getCreateIndex() {
        return createIndex;
    }

    public long getModifyIndex() {
        return modifyIndex;
    }
}
